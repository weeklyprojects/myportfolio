//
//  AnimationPartTwoApp.swift
//  AnimationPartTwo
//
//  Created by shreejwal giri on 31/05/2021.
//

import SwiftUI
@main
struct AnimationPartTwoApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
