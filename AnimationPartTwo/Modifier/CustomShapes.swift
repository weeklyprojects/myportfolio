//
//  CustomShapes.swift
//  AnimationPartTwo
//
//  Created by shreejwal giri on 31/05/2021.
//

import SwiftUI

struct BottomRoundedCorner: Shape {
    var  cornerRadius: CGFloat
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        return Path(path.cgPath)
    }
}

struct TopRoundedCorner: Shape {
    var  cornerRadius: CGFloat
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        return Path(path.cgPath)
    }
}
