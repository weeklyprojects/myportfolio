//
//  CustomViewModifier.swift
//  AnimationPartTwo
//
//  Created by shreejwal giri on 31/05/2021.
//

import SwiftUI

struct CustomViewModifier: View {
    var body: some View {
        Rectangle()
            .fill(Color.green)
            .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
}

struct leftDrawerProfileView: ViewModifier {
    
    func body(content: Content) -> some View {
        return content.overlay(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=Overlay Content@*/Text("Placeholder")/*@END_MENU_TOKEN@*/)
    }
    
    struct viewShape: Shape {
        func path(in rect: CGRect) -> Path {
            var shapePath = Path()
            shapePath.move(to: CGPoint(x: 0, y: 0))
            shapePath.addLine(to: CGPoint(x: rect.width, y: rect.height))
            return shapePath
        }
    }
}

struct CustomViewModifier_Previews: PreviewProvider {
    static var previews: some View {
        CustomViewModifier()
    }
}
