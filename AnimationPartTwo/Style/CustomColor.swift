//
//  CustomColor.swift
//  AnimationPartTwo
//
//  Created by shreejwal giri on 31/05/2021.
//

import Foundation
import SwiftUI

extension Color {
    static let primaryColor = Color(#colorLiteral(red: 0.9333333333, green: 0.8039215686, blue: 0.6392156863, alpha: 1))
    static let secondaryColor = Color(#colorLiteral(red: 0.937254902, green: 0.3843137255, blue: 0.6235294118, alpha: 1))
    static let iconDark = Color(#colorLiteral(red: 0.370555222, green: 0.3705646992, blue: 0.3705595732, alpha: 1))
}
