//
//  CustomFont.swift
//  AnimationPartTwo
//
//  Created by shreejwal giri on 03/06/2021.
//

import SwiftUI

extension Font {
    static func nexaBold(size: CGFloat? = 16) -> Font {
        return Font.custom("NexaBold", size: size!)
    }
    
    static func nexaLight(size: CGFloat? = 16) -> Font {
        return Font.custom("NexaLight", size: size!)
    }
}
