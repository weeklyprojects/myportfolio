//
//  LeftDrawer.swift
//  AnimationPartTwo
//
//  Created by shreejwal giri on 31/05/2021.
//

import SwiftUI

struct LeftDrawerContent: Identifiable {
    let id = UUID()
    let title: String
    let description: String
    let icon: String
}

struct HomeView: View {
    @State var showLeftDrawer: Bool = false
    
    var body: some View {
        GeometryReader { geometry in
            NavigationView {
                ZStack(alignment: .leading, content: {
                    Color.clear
                        .navigationBarTitle(Text(""), displayMode: .inline)
                        .navigationBarItems(leading: Button(action: {
                            withAnimation(.easeInOut(duration: 0.5)) {
                                showLeftDrawer.toggle()
                            }
                        }, label: {
                            Image(systemName: "circle.grid.cross.right.fill")
                                .rotationEffect(.degrees(showLeftDrawer ? 90 : 0))
                                .animation(.easeInOut(duration: 1))
                        }))
                    
                    ZStack {
                        PortfolioView(geometry: geometry.size)
                    }
                    .offset(x: showLeftDrawer ? geometry.size.width / 1.5 : 0)
                    
                    ZStack(alignment: .leading, content: {
                        LeftNavigationView(geometrySize: geometry.size)
                            .transition(.move(edge: .leading))
                            .offset(x: showLeftDrawer ? 0 : -geometry.size.width)
                    })
                })
                .foregroundColor(.black)
                .onTapGesture {
                    withAnimation(.easeInOut(duration: 0.5)) {
                        showLeftDrawer = false
                    }
                }
            }
        }
    }
}

struct LeftNavigationView: View {
    @State var geometrySize: CGSize
    @State var showLogoutAlert: Bool = false
    
    var leftViewContentArr: [LeftDrawerContent] = [
        LeftDrawerContent(title: "Mobile Number", description: "+977 XXXXXXXXXX", icon: "ic-call"),
        LeftDrawerContent(title: "Emial", description: "Shreejwalgiri2052@gmail.com", icon: "ic-email"),
        LeftDrawerContent(title: "Linked In", description: "shreejwal-giri-a60591128", icon: "ic-linkedin")
    ]
    
    var frameWidth: CGFloat {
        return geometrySize.width / 1.5
    }
    var body: some View {
        ZStack {
            VStack {
                Rectangle()
                    .frame(width: frameWidth, alignment: .leading)
                    .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                    .overlay(
                        VStack {
                            ZStack {
                                Rectangle()
                                    .fill(LinearGradient(gradient: Gradient(colors: [Color.primaryColor, Color.secondaryColor]), startPoint: .topLeading, endPoint: .bottomTrailing))
                                    .frame(width: frameWidth, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                    .clipShape(BottomRoundedCorner(cornerRadius: 30))
                                    .shadow(radius: 2)
                                
                                VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 5, content: {
                                    Image("pp")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: 120, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        .clipShape(Circle())
                                        .shadow(radius: 4)
                                    
                                    VStack {
                                        Text("Shreejwal Giri")
                                            .font(.nexaBold(size: 22))
                                        Text("IOS App Developer")
                                            .fontWeight(.medium)
                                            .font(.system(size: 12))
                                    }
                                    .foregroundColor(.white)
                                })
                            }
                            Spacer()
                            ScrollView(/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
                                VStack(alignment: .leading, spacing: nil, content: {
                                    ForEach(leftViewContentArr) { content in
                                        LeftDrawerContentView(leftDrawerContent: content)
                                    }
                                })
                            })
                            
                            Button(action: {
                                showLogoutAlert = true
                               
                            }, label: {
                                VStack(alignment: .leading, spacing: 20, content: {
                                    Divider().background(Color.black)
                                
                                    HStack(alignment: .center, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                                        Image("ic-logout")
                                            .resizable()
                                            .frame(width: 20, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        Text("Logout")
                                            .fontWeight(.semibold)
                                            .font(.system(size: 16))
                                    })
                                    .foregroundColor(.black)
                                    .padding(.leading, 20)
                                })
                                .padding([.bottom, .top], 40)

                            })
                        }
                        .background(Color.white)
                        .alert(isPresented: $showLogoutAlert, content: {
                            Alert(
                                title: Text("Logout").font(.nexaBold()),
                                message: Text("are you sure! you want to logout".capitalized),
                                primaryButton: .default(Text("Yes"), action: { exit(0)}
                                ), secondaryButton: .cancel()
                            )
                        })
                    )
            }
        }
        .frame(width: frameWidth, alignment: .leading)
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct LeftDrawerContentView: View {
    @State var leftDrawerContent: LeftDrawerContent
    var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            Spacer()
            HStack(alignment: .center, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                Image(leftDrawerContent.icon)
                    .resizable()
                    .frame(width: 20, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .foregroundColor(.iconDark)
                Text(leftDrawerContent.title)
                    .font(.nexaBold())
            })
            HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                Image(systemName: "")
                    .resizable()
                    .frame(width: 20, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                Text(leftDrawerContent.description)
                    .fontWeight(.light)
                    .font(.system(size: 12))
            })
            Spacer()
            
            Divider().background(Color.black)
            
        })
        .frame(height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        .padding([.leading, .trailing], 16)
        
    }
}

struct LeftDrawer_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

struct LeftNV_Previews: PreviewProvider {
    static var previews: some View {
        LeftNavigationView(geometrySize: .init(width: 500, height: 500))
    }
}
