//
//  PortfolioView.swift
//  AnimationPartTwo
//
//  Created by shreejwal giri on 01/06/2021.
//

import SwiftUI

struct PortfolioList: Identifiable {
    let id = UUID()
    let numbering: Int
    let title: String
    let titleList: String
    let backgroundIcon: String
}

struct PortfolioView: View {
    @State var geometry: CGSize
    @State var numbering: Int = 0
    
    var portfolioTitle = "Hi, I'm Shreejwal and I'm IOS App Developer"
    
    var portfolioDescription = "I Have experience developing numerous Fin-Tech based applications.​ I am familiar with CI/CD process and have been practicing agile framework, I am well experienced with tools such as kanban and jira."
    
    var portfolioListArr = [
        PortfolioList(numbering: 0, title: "Experience", titleList: "Software Engineer at F1 Soft International Pvt.Ltd", backgroundIcon: "ic-work"),
        PortfolioList(numbering: 1, title: "Education", titleList: "Graduated in BSc Computing from The British College", backgroundIcon: "ic-graduation"),
    ]
    
    var body: some View {
        
        ZStack(alignment: .center, content: {
            VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                VStack(alignment: .leading, spacing: 12, content: {
                    Text(portfolioTitle)
                        .font(.nexaBold(size: 25))
                    Text(portfolioDescription)
                        .fontWeight(.light)
                        .font(.system(size: 14))
                })
                .padding()
                
                ZStack {
                    Rectangle()
                        .fill(Color.white)
                        .clipShape(TopRoundedCorner(cornerRadius: 35))
                        .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                    
                    VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                        Divider().background(Color.black)
                            .frame(width: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        Divider().background(Color.black)
                            .frame(width: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        Text("My Portfolio")
                            .font(.nexaBold())
                        Spacer()
                        
                        ScrollView(/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
                            VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                                ForEach(portfolioListArr) { list in
                                    if list.numbering == 0 {
                                        PortfolioListViewLeft(portfolioListData: list)
                                    } else {
                                        PortfolioListViewRight(portfolioListData: list)
                                    }
                                }
                            })
                        })
                        
                        Spacer()
                    })
                    .padding(.top, 40)
                }
            })
            .edgesIgnoringSafeArea(.bottom)
        })
        
    }
}

struct PortfolioListViewLeft: View {
    @State var portfolioListData: PortfolioList
    var body: some View {
        ZStack(alignment: .trailing, content: {
            Rectangle()
                .fill(Color.white)
                
                .cornerRadius(10)
                .frame(height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .shadow(radius: 4)
            
            HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                Image(portfolioListData.backgroundIcon)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 180, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
                
                VStack(alignment: .leading, spacing: 10, content: {
                    Text(portfolioListData.title)
                        .font(.nexaBold())
                        .underline(/*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/, color: /*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/).bold()
                    Text(portfolioListData.titleList)
                        .font(.nexaLight())
                    
                    
                    Spacer()
                })
                .padding(.leading, 20)
                .padding(.top, 15)
                .padding(.trailing, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
            })
        })
        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, 18)
    }
}

struct PortfolioListViewRight: View {
    @State var portfolioListData: PortfolioList
    var body: some View {
        ZStack(alignment: .trailing, content: {
            Rectangle()
                .fill(Color.white)
                
                .cornerRadius(10)
                .frame(height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .shadow(radius: 4)
            
            HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                
                VStack(alignment: .leading, spacing: 10, content: {
                    Text(portfolioListData.title)
                        .font(.nexaBold())
                        .underline(/*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/, color: /*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/).bold()
                    Text(portfolioListData.titleList)
                        .font(.nexaLight())
                    
                    
                    Spacer()
                })
                .padding(.leading, 20)
                .padding(.top, 15)
                .padding(.trailing, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                
                Image(portfolioListData.backgroundIcon)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 180, height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            })
        })
        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, 18)
    }
}

struct PortfolioView_Previews: PreviewProvider {
    static var previews: some View {
        PortfolioView(geometry: CGSize(width: 600, height: 400), numbering: 0)
    }
}
