# MyPortfolio

# MyPortfolio

Creating a Single Layout Portfolio App.

- Contains 
    - [x] Side Menu Animation
    - [x] Use of ViewModifier And Extentions in SwiftUI 
    - [x] Use of Animation and Transactions.
    - [x] Use of Lunch Screen and Custom Fonts.
    - [x] Use of Alert in SwiftUI.

<img src="myPortfolioAppGif.gif" width="250" height="523" />


